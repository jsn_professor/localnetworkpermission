
# Installation
To integrate LocalNetworkPermission into your Xcode project using CocoaPods, specify it in your `Podfile`:

	source 'https://bitbucket.org/jsn_professor/podspec.git'
	source 'https://cdn.cocoapods.org'
	target 'MyApplication' do
		use_frameworks!
		
		# Pods for MyApplication
		pod 'LocalNetworkPermission'
	end

Add `NSLocalNetworkUsageDescription` property to specify usage description. The system will provide default description if omitted.

# Usage
	class ViewController: UIViewController {
		override func viewDidLoad() {
			checkLocalNetworkPermission(
                    successHandler: {
                    
                    },
                    failHandler: {
	                    isInitialRequest in
	                    //Parameter `isInitialRequest` will be true if user rejects access via system alert.
                    })
		}
	}
