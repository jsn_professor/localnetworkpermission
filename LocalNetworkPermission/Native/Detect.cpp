//
//  Detect.cpp
//  DDP
//
//  Created by Barkley on 2020/8/3.
//  Copyright © 2020 Barkley. All rights reserved.
//

#include "Detect.h"

int DetectLocalNetworkPermission(char *pDestAddr) {
    CDDPSocket *pSocket = new CDDPSocket();
    if (pSocket == NULL)
        return DETECT_API_SOCKET_CREATE_ERROR;

    if (pSocket->Create() != SOCKET_SUCCESS) {
        delete pSocket;
        pSocket = NULL;
        return DETECT_API_SOCKET_CREATE_ERROR;
    }

    BYTE buffer[1];
    memset(buffer, 1, sizeof(buffer));
    if (pSocket->Send(buffer, 1, pDestAddr) <= 0) {
        pSocket->Destroy();
        delete pSocket;
        pSocket = NULL;
        return DETECT_API_SOCKET_SEND_ERROR;
    }

    pSocket->Destroy();
    delete pSocket;
    pSocket = NULL;

    return DETECT_API_SUCCESS;
}
