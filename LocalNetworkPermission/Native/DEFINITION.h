//
//  DEFINITION.h
//  Detect
//
//  Created by Barkley on 2020/8/4.
//  Copyright © 2020年 Barkley. All rights reserved.
//

/*---------------------------------------------------------------------------------------------------*/
// Detect API result
#define DETECT_API_SUCCESS                     0
#define DETECT_API_SOCKET_CREATE_ERROR         -1
#define DETECT_API_SOCKET_SEND_ERROR           -2
/*---------------------------------------------------------------------------------------------------*/
// common define
typedef unsigned char BYTE;
typedef BYTE *LPBYTE;
typedef void *HANDLE;
typedef unsigned long DWORD;

#ifndef BOOL
#define BOOL int
#endif

#define TRUE    1
#define FALSE   0
