//
//  DDPSocket.cpp
//  DDP
//
//  Created by Barkley on 2018/2/27.
//  Copyright © 2018年 Barkley. All rights reserved.
//

#include "DDPSocket.h"

CDDPSocket::CDDPSocket(void) {
    m_hSocket = -1;
    memset(&m_mreq, 0, sizeof(ip_mreq));
    m_bJoinMulticast = FALSE;
}

CDDPSocket::~CDDPSocket(void) {

}

int CDDPSocket::Create() {
    if (m_hSocket != -1)
        return SOCKET_USED;

    m_hSocket = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (m_hSocket == -1)
        return SOCKET_ERROR;

    struct sockaddr_in saAddr;
    memset(&saAddr, 0, sizeof(saAddr));
    saAddr.sin_family = AF_INET;

    saAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    m_mreq.imr_interface.s_addr = htonl(INADDR_ANY);

    saAddr.sin_port = htons(DETECT_SERVER_LISTEN_PORT);

    BOOL bValue = TRUE;
    if (setsockopt(m_hSocket, SOL_SOCKET, SO_REUSEADDR, &bValue, sizeof(bValue)) == -1) {
        Destroy();
        return SOCKET_ERROR;
    }

    if (bind(m_hSocket, (struct sockaddr *) &saAddr, sizeof(saAddr)) == -1) {
        Destroy();
        return SOCKET_ERROR;
    }

    m_mreq.imr_multiaddr.s_addr = inet_addr("224.0.0.1");
    if (setsockopt(m_hSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *) &m_mreq, sizeof(m_mreq)) == -1) {
        Destroy();
        return SOCKET_ERROR;
    }

    m_bJoinMulticast = TRUE;

    return SOCKET_SUCCESS;
}

long CDDPSocket::Send(LPBYTE lpBuffer, DWORD dwSize, char *pDestAddr) {
    if (m_hSocket == -1)
        return SOCKET_ERROR;

    long iRet = SOCKET_ERROR;

    struct sockaddr_in toAddr;
    memset(&toAddr, 0, sizeof(toAddr));
    toAddr.sin_family = AF_INET;
    toAddr.sin_addr.s_addr = inet_addr(pDestAddr);
    toAddr.sin_port = htons(DETECT_CLIENT_LISTEN_PORT);
    iRet = sendto(m_hSocket, lpBuffer, dwSize, 0, (const sockaddr *) &toAddr, sizeof(toAddr));

    return iRet;
}

void CDDPSocket::Destroy() {
    if (m_hSocket != -1) {
        if (m_bJoinMulticast == TRUE)
            setsockopt(m_hSocket, IPPROTO_IP, IP_DROP_MEMBERSHIP, (char *) &m_mreq, sizeof(m_mreq));

        close(m_hSocket);
        m_hSocket = -1;
    }
}

