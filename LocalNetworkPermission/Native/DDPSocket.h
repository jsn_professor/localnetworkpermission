//
//  DDPSocket.h
//  DDP
//
//  Created by Barkley on 2018/2/27.
//  Copyright © 2018年 Barkley. All rights reserved.
//

#pragma once

#include "DEFINITION.h"
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

#define DETECT_CLIENT_LISTEN_PORT 0xF650
#define DETECT_SERVER_LISTEN_PORT 0xF660

// socket error
#define SOCKET_SUCCESS  0
#define SOCKET_ERROR    -1
#define SOCKET_USED     -99

#if defined __cplusplus

class CDDPSocket {
public:
    CDDPSocket(void);

    virtual ~CDDPSocket(void);

    int Create();

    long Send(LPBYTE lpBuffer, DWORD dwSize, char *pDestAddr);

    void Destroy();

protected:
    int m_hSocket;
    struct ip_mreq m_mreq;
    BOOL m_bJoinMulticast;
};

#endif
