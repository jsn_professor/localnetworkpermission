//
//  Detect.h
//  DDP
//
//  Created by Barkley on 2020/8/3.
//  Copyright © 2020 Barkley. All rights reserved.
//

#include <stdio.h>
#include "DEFINITION.h"
#include "DDPSocket.h"

#ifdef __cplusplus
extern "C"
{
#endif

int DetectLocalNetworkPermission(char *pDestAddr);

#ifdef __cplusplus
};
#endif
