//
//  UIViewControllerExtension.swift
//  LocalNetworkPermission
//
//  Created by Professor on 2020/8/4.
//  Copyright © 2020 Professor. All rights reserved.
//

import UIKit

private var timer: Timer?

extension UIViewController {
    @objc public func checkLocalNetworkPermission(successHandler: (() -> Void)? = nil, failHandler: ((_ isInitialRequest: Bool) -> Void)? = nil) {
        if #available(iOS 14.0, *) {
            if timer?.isValid == true {
                return
            }
            let start: UInt32 = 2851995649
            let end: UInt32 = 2852061182
            var observer: NSObjectProtocol?
            let determined = UserDefaults.standard.bool(forKey: "\(Bundle.main.bundleIdentifier ?? "")NSLocalNetworkUsageDescription")
            if !determined {
                UserDefaults.standard.set(true, forKey: "\(Bundle.main.bundleIdentifier ?? "")NSLocalNetworkUsageDescription")
                observer = NotificationCenter.default.addObserver(forName: UIApplication.willResignActiveNotification, object: nil, queue: .main) { notification in
                    timer?.invalidate()
                    timer = nil
                    NotificationCenter.default.removeObserver(observer as Any)
                    observer = NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: .main) { notification in
                        NotificationCenter.default.removeObserver(observer as Any)
                        if detectLocalNetworkPermission(addresses: Array((start...end).shuffled().prefix(16))) {
                            successHandler?()
                        } else {
                            failHandler?(true)
                        }
                    }
                }
            }
            let result = DetectLocalNetworkPermission(UnsafeMutablePointer<Int8>(mutating: (start.ipV4 as NSString).utf8String))
            if determined {
                if result == DETECT_API_SUCCESS {
                    successHandler?()
                } else {
                    if detectLocalNetworkPermission(addresses: Array((start + 1...end).shuffled().prefix(15))) {
                        successHandler?()
                    } else {
                        failHandler?(true)
                    }
                }
            } else {
                if result == DETECT_API_SUCCESS {
                    NotificationCenter.default.removeObserver(observer as Any)
                    successHandler?()
                } else {
                    timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false) {
                        _ in
                        timer?.invalidate()
                        timer = nil
                        NotificationCenter.default.removeObserver(observer as Any)
                        if result == DETECT_API_SUCCESS {
                            successHandler?()
                        } else {
                            if detectLocalNetworkPermission(addresses: Array((start + 1...end).shuffled().prefix(15))) {
                                successHandler?()
                            } else {
                                failHandler?(true)
                            }
                        }
                    }
                }
            }
        } else {
            successHandler?()
        }
    }
}

private func detectLocalNetworkPermission(addresses: [UInt32]) -> Bool {
    for address in addresses {
        if DetectLocalNetworkPermission(UnsafeMutablePointer<Int8>(mutating: (address.ipV4 as NSString).utf8String)) == DETECT_API_SUCCESS {
            return true
        }
    }
    return false
}

private extension UInt32 {
    var ipV4: String {
        var int = self
        var values = [String]()
        while values.count < 4 {
            values.append("\(int & 255)")
            int = int >> 8
        }
        return values.reversed().joined(separator: ".")
    }
}

